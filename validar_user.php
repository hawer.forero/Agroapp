<?php
require 'Users.php';
header("Access-Control-Allow-Origin: *");
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$email = $_POST['email'];
	$password = $_POST['password'];
	
	
 
	$retorno = Users::validarUser($email, $password);


	if ($retorno) {

		$meta["estado"] = true;
		$meta["user"] = $retorno;
		
		print json_encode($meta);
	} else {
		
		print json_encode(
			array(
				'estado' => false,
				'mensaje' => 'No se obtuvo el registro'
			)
		);
	}

    
}