$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});

$("#envio").on('click', function (e) {
  
  var r = true;  
  if ($('#nombre').val() == "") {
	   $('#mensaje1').show();
       r = false;
  }
  else{
	   $('#mensaje1').hide();
  }
  if ($('#apellido').val() == "") {
	   $('#mensaje2').show();
        r = false;
  }
   else{
	   $('#mensaje2').hide();
  }
  if ($('#email').val() == "") {
	   $('#mensaje3').show();
       r = false;
  }
   else{
	   $('#mensaje3').hide();
  }
  if ($('#password').val() == "") {
	   $('#mensaje4').show();
       r = false;
  }
   else{
	   $('#mensaje4').hide();
  }
 
   return r;
});







