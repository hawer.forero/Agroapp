<?php


require 'Database.php';

class Users
{
    function __construct()
    {
    }

   
    public static function getAll()
    {
        $consulta = "SELECT * FROM users";
        try {
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->execute();

            return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return false;
        }
    }

   
    public static function getById($id)
    {
        $consulta = "SELECT id,
                            nombre,
                             apellido,
                             email,
                             password
                             FROM users
                             WHERE id = ?";

        try {
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->execute(array($id));
            $row = $comando->fetch(PDO::FETCH_ASSOC);
            return $row;

        } catch (PDOException $e) {
            return -1;
        }
    }

    public static function update(
        $id,
        $nombre,
        $apellido,
        $email,
        $password
    )
    {
        $consulta = "UPDATE users" .
            " SET nombre=?, apellido=?, email=?, password=?" .
            "WHERE id=?";

        $cmd = Database::getInstance()->getDb()->prepare($consulta);

        $cmd->execute(array($nombre, $apellido, $email, $password, $id));

        return $cmd;
    }

    public static function insert(
        $nombre,
        $apellido,
        $email,
        $password
    )
    {
		
		try {
			$comando = "INSERT INTO users ( " .
				"nombre," .
				" apellido," .
				" email," .
				" password)" .
				" VALUES( ?,?,?,?)";

			$sentencia = Database::getInstance()->getDb()->prepare($comando);

			return $sentencia->execute(
				array(
					$nombre,
					$apellido,
					$email,
					$password
				)
			);

       } catch (PDOException $e) {
          return false;
       }
       

    }

    public static function delete($id)
    {
        $comando = "DELETE FROM users WHERE id=?";

        $sentencia = Database::getInstance()->getDb()->prepare($comando);

        return $sentencia->execute(array($id));
    }
	
	
    public static function validarUser($email, $password)
    {
        $consulta = "SELECT id,
                            nombre,
                             apellido,
                             email,
                             password
                             FROM users
                             WHERE email = ?
							 AND password = ?";

        try {
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            $comando->execute(array($email, $password));
            $row = $comando->fetch(PDO::FETCH_ASSOC);
            return $row;

        } catch (PDOException $e) {
            return -1;
        }
    }
}

?>