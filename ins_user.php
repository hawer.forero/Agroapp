<?php

require 'Users.php';
header("Access-Control-Allow-Origin: *");
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	$nombre = $_POST['nombre'];
	$apellido = $_POST['apellido'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	
    $retorno = Users::insert(
        $nombre,
        $apellido,
        $email,
        $password);

    if ($retorno) {
        // Código de éxito
        print json_encode(
            array(
                'estado' => true,
                'mensaje' => 'Creación exitosa')
        );
    } else {
        // Código de falla
        print json_encode(
            array(
                'estado' => false,
                'mensaje' => 'Creación fallida')
        );
    }
}