<?php


require 'Plantas.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    // Manejar petición GET
    $plantas = Plantas::getAll();

    if ($plantas) {

        $datos["estado"] = true;
        $datos["plantas"] = $plantas;

        print json_encode($datos);
    } else {
        print json_encode(array(
            "estado" => false,
            "mensaje" => "Ha ocurrido un error"
        ));
    }
}