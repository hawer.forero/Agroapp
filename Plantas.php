<?php

require 'Database.php';

class Plantas
{
    function __construct()
    {
    }

    public static function getAll()
    {
        $consulta = "SELECT * FROM plantas";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute();

            return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return false;
        }
    }
 
}

?>