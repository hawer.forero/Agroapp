
function plantas(){
			
    $.ajax({
            url: 'http://localhost/agroapp/get_plantas.php',
            type: 'get',
            dataType: 'json',
            success: function (data) {
				if(data.estado)
				{
                     $('#cargando').addClass( "cargando" );
                      setTimeout(function() {
									$('#cargando').removeClass( "cargando" );
                                     html = mostrarPlantas(data.plantas);
                                    $("#plantas").html(html);
								},3000); 
                   
				}
				else{
					 
                    
					 
				}
            },
        });
}
	
	
function mostrarPlantas(data){
   
var html =  '<h3 class="text-center">SELECCIONE UNA PLANTA:</h3>'+
			'<p class="text-center"></p>';
    for(var i=0; i<data.length; i++){
       
         html += 
       
			'<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 ">'+
				'<div class="he-wrap tpl6 ">'+
					'<img src="'+data[i].imagen+'" alt="Vegetable Farm" title="Vegetable Farm">'+
					'<div class="he-view">'+
						'<div class="bg a0" data-animate="fadeIn">'+
							'<h3 class="a1" data-animate="fadeInDown">Iniciar</h3>'+
							'<a onclick="cuestionario('+data[i].id+')" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-youtube-play"></i></a>'+
						'</div>'+
					'</div>'+	
				'</div>'+
				'<h4 class="">'+data[i].nombre+'</h4>'+
			'</div>';	
        
        
        
    }
    return html;
}
function cuestionario(id){
    //alert("hola");
    var data = {
            planta_id: id,
        }
        $.ajax({
            url: 'http://localhost/agroapp/get_cuestionario.php',
            type: 'post',
			data: data,
            dataType: 'json',
            success: function (data) {
				if(data.estado)
				{
					 $('#cargando').addClass( "cargando" );
                      setTimeout(function() {
									$('#cargando').removeClass( "cargando" );
                                     html = mostrarCuestionario(data.cuestionario);
                                    $("#plantas").html(html);
								},3000); 
                      
				}
				else{
					
					 
				}
            },
        });
}

function mostrarCuestionario(data){
   
var html =  '<h3 class="text-center">Diagnostico cultivo de platano</h3>'+
			'<div class="typography">'+
            '<div class="container">';
    for(var i=0; i<data.length; i++){
       
         html += 
       
				'<div class="grid_3 grid_4">'+
					'<h3 class="hdg">'+(i+1)+'. '+data[i].nombre+'</h3>'+
					'<div class="bs-example">'+
						'<table class="table table-bordered">'+
                             '<thead>'+
								'<tr>'+
									'<th>Respuesta</th>'+
									'<th>Opción</th>'+
								'</tr>'+
							 '</thead>'+
							'<tbody>';
                    var op = data[i].opciones;
                    for(var j=0; j<op.length; j++){
                        
						 html += '<tr>'+
									'<td>'+op[j].nombre+'</td>'+
									'<td><input type="radio" name="p'+data[i].id+'" value="'+op[j].valor+'"></td>'+
								'</tr>';
                }
        html += 					
							'</tbody>'+
						'</table>'+
					'</div>'+
				'</div>';

		  }		
                                    
     html +=  '</div>'+
			'</div>';
            
	
		
        
        
        
  
    return html;
}
