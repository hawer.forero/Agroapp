$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
}); 

function login() {
	if(login_valide())
	{
	
        var data = {
            email: $("#email").val(),
			password:$("#password").val()
        }

        $.ajax({
            url: 'http://localhost/agroapp/validar_user.php',
            type: 'post',
			data: data,
            dataType: 'json',
            success: function (data) {
				if(data.estado)
				{
					 //$('#target').html(data.mensaje);
					 //window.location="home.html";
                    
                     $('#cargando').addClass( "cargando" );
                      setTimeout(function() {
									$('#cargando').removeClass( "cargando" );
                                    window.location="home.html";
								},4000);
                      
				}
				else{
					 $('#cargando').addClass( "cargando" );
                     setTimeout(function() {
									$('#cargando').removeClass( "cargando" );
                                    $('#login-error').show();
								},2000);
					 
				}
            },
        });
	}else{return false}
}
function msg() {
	$('#login-error').show();
}

function insUser() {
	if(form_valide())
	{
        var user = {
            nombre: $("#nombre").val(),
            apellido:$("#apellido").val(),
            email: $("#email2").val(),
			password:$("#password2").val()
        }
		console.log(user);
        $.ajax({
            url: 'http://localhost/agroapp/ins_user.php',
            type: 'post',
			data: user,
            dataType: 'json',
            success: function (data) {
				if(data.estado)
				{
					$('#cargando').addClass( "cargando" );
                      setTimeout(function() {
									$('#cargando').removeClass( "cargando" );
									alert("Usuario registrado con exito")
                                    window.location="home.html";
								},4000);
				}
				else{
					 $('#cargando').addClass( "cargando" );
                     setTimeout(function() {
									$('#cargando').removeClass( "cargando" );
                                    $('#login-error2').show();
								},2000);
				}
            },
        });
		}
    else{
			return false;
    }
		
    }

function form_valide(){
			var r = true;  
	  if ($('#nombre').val() == "") {
		   $('#mensaje1').show();
		   r = false;
	  }
	  else{
		   $('#mensaje1').hide();
	  }
	  if ($('#apellido').val() == "") {
		   $('#mensaje2').show();
			r = false;
	  }
	   else{
		   $('#mensaje2').hide();
	  }
	  if ($('#email2').val() == "") {
		   $('#mensaje3').show();
		   r = false;
	  }
	   else{
		   $('#mensaje3').hide();
	  }
	  if ($('#password2').val() == "") {
		   $('#mensaje4').show();
		   r = false;
	  }
	   else{
		   $('#mensaje4').hide();
	  }
	 
	   return r;
		
}
function login_valide(){
			var r = true;  
	 
	  if ($('#email').val() == "") {
		   $('#mensaje3-1').show();
		   r = false;
	  }
	   else{
		   $('#mensaje3-1').hide();
	  }
	  if ($('#password').val() == "") {
		   $('#mensaje4-1').show();
		   r = false;
	  }
	   else{
		   $('#mensaje4-1').hide();
	  }
	 
	   return r;
		
}
	
	

