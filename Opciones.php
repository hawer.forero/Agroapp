<?php

//require 'Database.php';

class Opciones
{
    function __construct()
    {
    }

    public static function getAll($id)
    {
        $consulta = "SELECT * FROM opciones WHERE pregunta_id = ?";
        try {
            // Preparar sentencia
            $comando = Database::getInstance()->getDb()->prepare($consulta);
            // Ejecutar sentencia preparada
            $comando->execute(array($id));

            return $comando->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            return false;
        }
    }
    
}

?>