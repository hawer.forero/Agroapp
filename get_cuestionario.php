<?php


require 'Preguntas.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$planta_id = $_POST['planta_id'];

    // Manejar petición GET
    $preguntas = Preguntas::getAll($planta_id);

    if ($preguntas) {
        require 'Opciones.php';
        for($i = 0; $i < sizeof($preguntas); $i++)
        {
           //print $preguntas[$i]['id'];
            $opciones = Opciones::getAll($preguntas[$i]['id']);
            $p = array(
            "id" => $preguntas[$i]['id'],
            "nombre" => $preguntas[$i]['nombre'],
            "opciones" => $opciones
            );
            $cuestionario[$i]=$p;
        }

        $datos["estado"] = true;
        $datos["cuestionario"] = $cuestionario;

        print json_encode($datos);
    } else {
        print json_encode(array(
            "estado" => false,
            "mensaje" => "Ha ocurrido un error"
        ));
    }
}